let users = [];

function Advocate(id, state = null, reportingTo = null) {
  // this.name = name;
  this.id = id;
  this.senior = true;
  this.juniors = [];
  this.cases = [];
  this.rejectedCases = [];
  this.reportingTo = reportingTo;
  this.state = state;
  this.AddAdvocate = function() {
    console.log(`Advocate added ${this.id}`);
    return this;
  };
}

Advocate.prototype.AddJunior = function(junior) {
  users = users.filter(u => {
    if (u.juniors.indexOf(junior.id) == -1) {
      if (u.id == this.id) {
        this.juniors.push(junior.id);
        junior.reportingTo = u.id;
        console.log("Advocate " + junior.id + " added under " + this.id);
      }
    } else {
    }
    return this;
  });
};

Advocate.prototype.AddState = function(state) {
  users = users.filter(u => {
    u.id == this.id ? (this.state = state) : this.state;
    return this;
  });
};

Advocate.prototype.AddCases = function(cases) {
  users = users.filter(u => {
    if (u.id == this.id && !u.reportingTo) {
      this.cases.push(cases);
      console.log(`Case ${cases} added to ${this.id}`);
    } else if (u.id == this.id && u.reportingTo) {
      users = users.filter(ur => {
        if (ur.id == u.reportingTo) {
          if (ur.rejectedCases.indexOf(cases) == -1) {
            this.cases.push(cases);
            console.log(`Case ${cases} added to ${this.id}`);
          } else console.log(`Cannot add ${cases} case under ${this.id}`);
        }
      });
    }
    return this;
  });
};

Advocate.prototype.RejectCase = function(cases) {
  users = users.filter(u => {
    if (u.id == this.id && !u.reportingTo) {
      this.rejectedCases.push(cases);
      console.log(`Case ${cases} is added in Block list for ${this.id}`);
    }
    return this;
  });
};

const getCases = state => {
  users.filter(u => {
    u.state == state ? console.log(`${u.id}: ${u.cases.join(", ")}`) : ``;
  });
};

const getAllAdvocates = () => {
  users
    .filter(u => {
      console.log(`${u.id}`);
    })
    .sort();
};

const newAdvocate = new Advocate("Sooria", 1000);
const newAdvocate1 = new Advocate("Ram", 1001);
const newAdvocate2 = new Advocate("Nanda", 1002);
const newAdvocate3 = new Advocate("Suresh", 1003);
users = [newAdvocate, newAdvocate1, newAdvocate2, newAdvocate3];
newAdvocate.AddAdvocate();
newAdvocate1.AddAdvocate();
newAdvocate.AddJunior(newAdvocate1);
newAdvocate.AddState("TN");
newAdvocate1.AddState("KA");
newAdvocate.AddCases(505);
newAdvocate.AddCases(400);
newAdvocate.AddCases(404);
newAdvocate.AddCases(200);
newAdvocate.RejectCase(300);
newAdvocate1.AddCases(300);
newAdvocate1.AddCases(303);
newAdvocate2.AddAdvocate();
newAdvocate3.AddAdvocate();
newAdvocate2.AddState("TN");
newAdvocate3.AddState("TN");
newAdvocate2.AddCases("7/07");
newAdvocate2.AddCases("97");
newAdvocate2.AddCases("40/4");
newAdvocate3.AddCases("2/00");
getCases("TN");
getAllAdvocates();
// console.log(users);
